
package org.hlwd.bible;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchFragment extends Fragment
{
    private Context _context = null;
    private SCommon _s = null;
    private View v;
    private final FRAGMENT_TYPE fragmentType;
    private TextView tvBookTitle;
    private TextView btnBack;
    private TextView btnForward;
    private SimpleCursorAdapter cursorAdapter;
    private MatrixCursor matrixCursor;
    private String searchFullQuery;
    private int searchFullQueryLimit = 3;
    private SearchView searchView = null;
    @SuppressLint("StaticFieldLeak")
    private static RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private ArrayList<ShortSectionBO> lstArtShortSection;

    private boolean isBook = false,  isChapter = false,  isVerse = false,   isPrbl = false;
    private int     bNumber = 0,     cNumber = 0,        vNumber = 0,       scrollPosY = 0;
    private String  bbName = null, tabTitle = null, trad = null;

    private boolean isChapterListLoading = false;

    private int planId = -1, planDayNumber = -1, planPageNumber = -1;

    enum FRAGMENT_TYPE {
        FAV_TYPE,
        SEARCH_TYPE,
        ARTICLE_TYPE,
        PLAN_TYPE
    }

    enum CLIPBOARD_ADD_TYPE {
        VERSE,
        CHAPTER,
        ALL
    }

    @SuppressLint("ValidFragment")
    public SearchFragment(final FRAGMENT_TYPE type)
    {
        fragmentType = type;
    }

    static int GetScrollPosY()
    {
        final LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (lm == null)
            return 0;

        return lm.findFirstVisibleItemPosition();
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle bundle)
    {
        try
        {
            super.onCreateView(inflater, container, bundle);

            CheckLocalInstance();

            v = inflater.inflate(R.layout.fragment_search, container, false);
            setHasOptionsMenu(true);

            tvBookTitle = v.findViewById(R.id.tvBookTitle);
            btnBack = v.findViewById(R.id.btnBack);
            btnForward = v.findViewById(R.id.btnForward);
            btnBack.setOnClickListener(v -> {
                SetLocalBibleName();

                if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
                {
                    if (isBook && isChapter)
                    {
                        btnForward.setEnabled(true);

                        if (cNumber > 1)
                        {
                            scrollPosY = 0;
                            cNumber--;
                            searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                            ShowChapter(bbName, bNumber, cNumber);
                        }
                        else
                        {
                            if (bNumber > 1)
                            {
                                final int bNumberTry = bNumber - 1;
                                final int cNumberTry = _s.GetBookChapterMax(bNumberTry);
                                if (IsChapterExist(bbName, bNumberTry, cNumberTry))
                                {
                                    scrollPosY = 0;
                                    bNumber = bNumberTry;
                                    cNumber = cNumberTry;
                                    searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                                    ShowChapter(bbName, bNumber, cNumber);
                                }
                            }
                            else
                            {
                                btnBack.setEnabled(false);
                            }
                        }
                    }
                    else if (isPrbl)
                    {
                        btnForward.setEnabled(true);
                        ShowPrbl(-1);
                        btnBack.requestFocus();
                    }
                }
                else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
                {
                    btnForward.setEnabled(true);
                    ShowPlan(-1);
                }
                else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
                {
                    btnForward.setEnabled(true);
                    ShowArticle(-1);
                }
            });
            btnForward.setOnClickListener(v -> {
                SetLocalBibleName();

                if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
                {
                    if (isBook && isChapter)
                    {
                        btnBack.setEnabled(true);

                        int cNumberTry = cNumber + 1;
                        if (IsChapterExist(bbName, bNumber, cNumberTry))
                        {
                            scrollPosY = 0;
                            cNumber = cNumberTry;
                            searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                            ShowChapter(bbName, bNumber, cNumber);
                        }
                        else
                        {
                            final int bNumberTry = bNumber + 1;
                            cNumberTry = 1;
                            if (IsChapterExist(bbName, bNumberTry, cNumberTry))
                            {
                                scrollPosY = 0;
                                bNumber = bNumberTry;
                                cNumber = cNumberTry;
                                searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                                ShowChapter(bbName, bNumber, cNumber);
                            }
                            else
                            {
                                btnForward.setEnabled(false);
                            }
                        }
                    }
                    else if (isPrbl)
                    {
                        btnBack.setEnabled(true);
                        ShowPrbl(1);
                        btnForward.requestFocus();
                    }
                }
                else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
                {
                    btnBack.setEnabled(true);
                    ShowPlan(1);
                }
                else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
                {
                    btnBack.setEnabled(true);
                    ShowArticle(1);
                }
            });

            if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
            {
                //Search book autocomplete
                matrixCursor = new MatrixCursor(new String[]{"_id", "text"});
                String[] from = {"text"};
                int[] to = {android.R.id.text1};                                                                         //android.R.ciId.text1,  R.ciId.listBnameEntry

                cursorAdapter = new SimpleCursorAdapter(_context, PCommon.GetPrefThemeName(_context).compareTo("LIGHT") == 0 ? R.layout.book_entry_light : R.layout.book_entry_dark, matrixCursor, from, to, SimpleCursorAdapter.NO_SELECTION); //android.R.layout.select_dialog_item,      R.layout.book_entry)
                cursorAdapter.setFilterQueryProvider(new FilterQueryProvider()
                {
                    @Override
                    public Cursor runQuery(CharSequence query)
                    {
                        if (PCommon._isDebugVersion) System.out.println("RunQuery: " + query);

                        return GetCursor(query);
                    }
                });
            }

            ShowBookTitle(false);   //May change in resume
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (PCommon._isDebugVersion) System.out.println("SearchFragment: OnCreateView, tab:" + MainActivity.Tab.GetCurrentTabPosition());
        }

        return v;
    }

    @Override
    public void onResume()
    {
        try
        {
            super.onResume();

            CheckLocalInstance();
            SetLocalBibleName();

            if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                searchFullQueryLimit = 0;

                tabTitle = _context.getString(R.string.favHeader);
                searchFullQuery = null;
                scrollPosY = 0;
                isBook = false;
                isChapter = false;
                isVerse = false;
                bNumber = 0;
                cNumber = 0;
                vNumber = 0;
                trad = bbName;

                final CacheTabBO t = _s.GetCurrentCacheTab();
                if (t != null)
                {
                    searchFullQuery = t.fullQuery;
                    scrollPosY = t.scrollPosY;
                }

                //Set objects
                SetTabTitle(tabTitle);
                ShowBookTitle(false);
                CreateRecyclerView();

                final int favOrder = PCommon.GetFavOrder(getContext());
                final int favType = PCommon.GetFavFilter(getContext());
                recyclerViewAdapter = new BibleAdapter(getContext(), bbName, searchFullQuery, favOrder, favType);
                if (WhenTabIsEmptyOrNull(true)) return;
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                return;
            }
            else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
            {
                searchFullQueryLimit = 3;

                final CacheTabBO t = _s.GetCurrentCacheTab();
                if (t == null)
                {
                    WhenTabIsEmptyOrNull(false);
                    return;
                }

                tabTitle = t.tabTitle;
                searchFullQuery = t.fullQuery;
                scrollPosY = t.scrollPosY;
                bbName = t.bbName;
                isBook = t.isBook;
                isChapter = t.isChapter;
                isVerse = t.isVerse;
                bNumber = t.bNumber;
                cNumber = t.cNumber;
                vNumber = t.vNumber;
                trad = t.trad;

                @SuppressWarnings("UnusedAssignment") String title = null;
                final String[] cols = searchFullQuery.split("\\s");
                if (cols.length == 3)
                {
                    planId = Integer.parseInt(cols[0]);
                    planDayNumber = Integer.parseInt(cols[1]);
                    planPageNumber = Integer.parseInt(cols[2]);
                    PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.PLAN_ID, planId);
                    PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.PLAN_PAGE, planPageNumber);
                    final PlanDescBO pd = _s.GetPlanDesc(planId);
                    if (pd != null)
                    {
                        final PlanCalBO pc = _s.GetPlanCalByDay(bbName, planId, planDayNumber);
                        if (pc != null)
                        {
                            title = PCommon.ConcaT(getString(R.string.planCalTitleDay).toUpperCase(), " ", pc.dayNumber, "/", pd.dayCount, "  ");
                            tvBookTitle.setText(title);
                            ShowBookTitle(true, pc);
                        }
                    }
                    else
                    {
                        t.tabType = "S";
                        _s.SaveCacheTab(t);

                        ShowBookTitle(false);
                    }
                }

                //Set objects
                SetTabTitle(tabTitle);

                SearchBible(true);

                return;
            }
            else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
            {
                searchFullQueryLimit = 0;

                tabTitle = getString(R.string.tabTitleDefault);
                searchFullQuery = null;
                scrollPosY = 0;
                isBook = false;
                isChapter = false;
                isVerse = false;
                bNumber = 0;
                cNumber = 0;
                vNumber = 0;
                trad = bbName;

                final CacheTabBO t = _s.GetCurrentCacheTab();
                if (t != null)
                {
                    tabTitle = t.tabTitle;
                    searchFullQuery = t.fullQuery;
                    scrollPosY = t.scrollPosY;
                }

                //Set objects
                SetTabTitle(tabTitle);

                final int artId = PCommon.GetResId(_context, searchFullQuery);
                final String artTitle = searchFullQuery.startsWith("ART") ? _context.getString(artId).toUpperCase() : null;
                if (artTitle != null) tvBookTitle.setText(artTitle);

                if (searchFullQuery.compareTo("ART_APP_HELP") == 0 || searchFullQuery.compareTo("ART_APP_LOG") == 0) {
                    ShowBookTitle(PCommon.GetInstallStatus(_context) == 6);
                } else {
                    ShowBookTitle(searchFullQuery.startsWith("ART"));
                }

                CreateRecyclerView();

                //Show article
                final ArtOriginalContentBO artOriginalContent = GetArticle(t);
                recyclerViewAdapter = new BibleArticleAdapter(getActivity(), bbName, artOriginalContent);
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                lstArtShortSection = ((BibleArticleAdapter) recyclerViewAdapter).GetArticleShortSections();

                return;
            }

            //Search type
            searchFullQueryLimit = 3;

            //Get tab info
            final CacheTabBO t = _s.GetCurrentCacheTab();
            if (t == null)
            {
                WhenTabIsEmptyOrNull(false);
                return;
            }

            //tabNumber is critical => function
            tabTitle = t.tabTitle;
            searchFullQuery = t.fullQuery;
            scrollPosY = t.scrollPosY;
            bbName = t.bbName;
            isBook = t.isBook;
            isChapter = t.isChapter;
            isVerse = t.isVerse;
            bNumber = t.bNumber;
            cNumber = t.cNumber;
            vNumber = t.vNumber;
            trad = t.trad;

            //Set objects
            SetTabTitle(tabTitle);

            isPrbl = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && GetPrblName(searchFullQuery) != null;
            ShowBookTitle(isPrbl);

            if ((isBook && isChapter) || isPrbl)
            {
                SearchBible(false);
            }
            else
            {
                SearchBible(true);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (PCommon._isDebugVersion) System.out.println("SearchFragment: OnResume, tab:" + MainActivity.Tab.GetCurrentTabPosition());
        }
    }

    private boolean WhenTabIsEmptyOrNull(final boolean shouldCheckRecycler)
    {
        try
        {
            if (!shouldCheckRecycler)
            {
                CreateRecyclerView();
            }
            else
            {
                if (recyclerViewAdapter != null && recyclerViewAdapter.getItemCount() > 0)
                {
                    return false;
                }
            }

            final String content = getString(R.string.tabEmpty);
            final ArtOriginalContentBO artOriginalContent = new ArtOriginalContentBO("", content);
            recyclerViewAdapter = new BibleArticleAdapter(getActivity(), bbName, artOriginalContent);
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(0);

            return true;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return false;
    }

    @Override
    public void onCreateContextMenu(@NonNull final ContextMenu menu, @NonNull final View v, final ContextMenu.ContextMenuInfo menuInfo)
    {
        try
        {
            super.onCreateContextMenu(menu, v, menuInfo);

            final MenuInflater menuInflater = Objects.requireNonNull(getActivity()).getMenuInflater();
            menuInflater.inflate(R.menu.context_menu_search, menu);

            final int bibleId = Integer.parseInt(PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.BIBLE_ID, "0"));
            final VerseBO verse = (bibleId > -1) ? _s.GetVerse(bibleId) : null;
            final boolean bible_cmd_visibility = (bibleId > -1);
            menu.findItem(R.id.mnu_open).setVisible(bible_cmd_visibility);
            menu.findItem(R.id.mnu_clipboard).setVisible(bible_cmd_visibility);


            //~~~ Cross references
            final boolean cr_cmd_visibility = verse != null && (_s.GetCrossReferencesCount(verse.bNumber, verse.cNumber, verse.vNumber) > 0);
            menu.findItem(R.id.mnu_cr).setVisible(cr_cmd_visibility);


            //~~~ Listen
            final String listenPosMainTitle;
            final String[] arrListen = _s.GetListenPosition(v.getContext());
            if (arrListen == null || arrListen.length < 4)
            {
                listenPosMainTitle = null;
            }
            else
            {
                final String listen_bbname = arrListen[0];
                final int listen_bnumber = Integer.parseInt(arrListen[1]);
                final int listen_cnumber = Integer.parseInt(arrListen[2]);
                final BibleRefBO bookRef = _s.GetBookRef(listen_bbname, listen_bnumber);
                listenPosMainTitle = PCommon.ConcaT(bookRef.bName, " ", listen_cnumber);
            }
            final int listenStatus = PCommon.GetListenStatus(v.getContext());
            final String listenMainAction = listenStatus > 0 ? getString(R.string.mnuListenStop) : getString(R.string.mnuListen);
            final String listenMainTitle = listenPosMainTitle == null
                    ? getString(R.string.mnuListen)
                    : PCommon.ConcaT(listenMainAction, " (", listenPosMainTitle, ")");

            menu.findItem(R.id.mnu_listen).setTitle(listenMainTitle);


            //~~~ Edit items
            final int editStatus = PCommon.GetEditStatus(v.getContext());
            final int editArtId = PCommon.GetEditArticleId(v.getContext());
            //TODO NEXT: Create customized view -- final String editArtName = editStatus == 1 ? _s.GetMyArticleName(editArtId) : "";         //"<small>Un example</small>" : "";
            final int tabArtId = editStatus == 1 && fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE && tabTitle.startsWith(getString(R.string.tabMyArtPrefix))
                    ? Integer.parseInt(tabTitle.replaceAll(getString(R.string.tabMyArtPrefix), ""))
                    : -1;
            final String editTitle = editStatus == 0 ? getString(R.string.mnuEditOn) :
                        PCommon.ConcaT(getString(R.string.mnuEditOff),
                        " (",
                        getString(R.string.tabMyArtPrefix),
                        editArtId,
                        ")");

            final boolean edit_art_cmd_visibility = editStatus == 1 && editArtId == tabArtId;
            final boolean edit_search_cmd_visibility = editStatus == 1 && (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE || fragmentType == FRAGMENT_TYPE.PLAN_TYPE);
            final boolean edit_fav_cmd_visibility = editStatus == 1 && fragmentType == FRAGMENT_TYPE.FAV_TYPE;
            menu.findItem(R.id.mnu_edit_select_from).setVisible(edit_search_cmd_visibility);
            menu.findItem(R.id.mnu_edit_select_to).setVisible(edit_search_cmd_visibility);
            menu.findItem(R.id.mnu_edit_select_from_to).setVisible(edit_search_cmd_visibility || edit_fav_cmd_visibility);
            menu.findItem(R.id.mnu_edit_move).setVisible(edit_art_cmd_visibility);
            menu.findItem(R.id.mnu_edit_add).setVisible(edit_art_cmd_visibility);
            menu.findItem(R.id.mnu_edit_update).setVisible(edit_art_cmd_visibility);
            menu.findItem(R.id.mnu_edit_remove).setVisible(edit_art_cmd_visibility);


            //~~~ Article
            if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
            {
                menu.findItem(R.id.mnu_fav).setVisible(false);
            }


            //~~~ Listen & Edit
            final int installStatus = PCommon.GetInstallStatus(v.getContext());
            if (installStatus != 6)
            {
                menu.findItem(R.id.mnu_listen).setVisible(false);
                menu.findItem(R.id.mnu_edit).setVisible(false);
            }
            else
            {
                menu.findItem(R.id.mnu_listen).setVisible(true);
                menu.findItem(R.id.mnu_edit).setTitle(editTitle).setVisible(true);                  //.setActionView(textView);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull final MenuItem item)
    {
        try
        {
            final int itemId = item.getItemId();
            final int bibleId = Integer.parseInt(PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.BIBLE_ID, "0"));
            final int position = Integer.parseInt(PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.VIEW_POSITION, "0"));
            final VerseBO verse = (bibleId > -1) ? _s.GetVerse(bibleId) : null;

            if (itemId == R.id.mnu_open) {
                if (verse == null) return false;

                final String mnuOpenVerse = getString(R.string.mnuOpenVerse);
                final String mnuOpenChapter = getString(R.string.mnuOpenChapter);
                final String mnuOpenResult = getString(R.string.mnuOpenResult);

                final ArrayList<String> mnuOpen = new ArrayList<>();
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE) mnuOpen.add(mnuOpenVerse);
                mnuOpen.add(mnuOpenChapter);
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE && fragmentType != FRAGMENT_TYPE.FAV_TYPE)
                    mnuOpen.add(mnuOpenResult);

                final AlertDialog builderOpenMenu = new AlertDialog.Builder(Objects.requireNonNull(Objects.requireNonNull(getContext()))).create();
                ShowMenu(builderOpenMenu, R.string.mnuOpen, mnuOpen);
                builderOpenMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    final AlertDialog builder = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                    final LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
                    final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, (ViewGroup) Objects.requireNonNull(getActivity()).findViewById(R.id.llLanguages));

                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuOpenVerse) == 0) {
                            final String msg = PCommon.ConcaT(getString(R.string.mnuOpenVerse), "");
                            PCommon.SelectBibleLanguageMulti(builder, getContext(), vllLanguages, msg, "", true, false);
                            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    final Context dlgContext = ((AlertDialog) dialogInterface).getContext();
                                    final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                                    if (bbname.equals("")) return;
                                    final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                                    final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);
                                    MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                                }
                            });
                            builder.show();
                        } else if (mnu_dialog.compareTo(mnuOpenChapter) == 0) {
                            final String msg = PCommon.ConcaT(getString(R.string.mnuOpenChapter), "");
                            PCommon.SelectBibleLanguageMulti(builder, getContext(), vllLanguages, msg, "", true, false);
                            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    final Context dlgContext = ((AlertDialog) dialogInterface).getContext();
                                    final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                                    if (bbname.equals("")) return;
                                    final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber);
                                    final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);
                                    MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                                }
                            });
                            builder.show();
                        } else if (mnu_dialog.compareTo(mnuOpenResult) == 0) {
                            final int tabIdFrom = tabNumber();
                            if (tabIdFrom >= 0) {
                                final String msg = PCommon.ConcaT(getString(R.string.mnuOpenResult), "");
                                PCommon.SelectBibleLanguageMulti(builder, getContext(), vllLanguages, msg, "", true, false);
                                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        final Context dlgContext = ((AlertDialog) dialogInterface).getContext();
                                        final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                                        if (bbname.equals("")) return;
                                        final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);

                                        if (isVerse) {
                                            final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                                            MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                                        } else if (isChapter) {
                                            final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber);
                                            MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                                        } else {
                                            MainActivity.Tab.AddTab(dlgContext, tabIdFrom, tbbName, verse.vNumber);
                                        }
                                    }
                                });
                                builder.show();
                            }
                        }
                    }
                });
            } else if (itemId == R.id.mnu_clipboard) {
                if (verse == null) return false;

                class InnerClass {
                    final Context cmContext = getContext();

                    void ClearClipboard() {
                        try {
                            PCommon.SaveClipboardIds(cmContext, new ArrayList<>());
                            PCommon.CopyTextToClipboard(cmContext, "", null, false);
                            PCommon.ShowToast(cmContext, R.string.toastEmpty, Toast.LENGTH_SHORT);
                        } catch (Exception ex) {
                            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
                        }
                    }

                    /***
                     * Add to clipboard
                     * @param addType   Type of add
                     * @param lstAllId  Used only with type ALL
                     */
                    void AddToClipboard(final CLIPBOARD_ADD_TYPE addType, final ArrayList<Integer> lstAllId) {
                        try {
                            //Manage IDs
                            ArrayList<Integer> lstIdGen = PCommon.GetClipboardIds(cmContext);
                            ArrayList<Integer> lstVersesId;
                            if (addType == CLIPBOARD_ADD_TYPE.VERSE || addType == CLIPBOARD_ADD_TYPE.CHAPTER) {
                                final VerseBO currentVerse = _s.GetVerse(bibleId);
                                if (currentVerse == null) return;
                                final String bbname = currentVerse.bbName;
                                final int bnumber = currentVerse.bNumber;
                                final int cnumber = currentVerse.cNumber;
                                final int vnumber = currentVerse.vNumber;

                                if (addType == CLIPBOARD_ADD_TYPE.VERSE) {
                                    lstVersesId = _s.GetVersesId(bbname, bnumber, cnumber, vnumber, vnumber);
                                } else {
                                    lstVersesId = _s.GetVersesId(bbname, bnumber, cnumber, 1, 0);
                                }
                            } else {
                                lstVersesId = lstAllId;
                            }
                            for (Integer verseId : lstVersesId) {
                                if (!lstIdGen.contains(verseId)) lstIdGen.add(verseId);
                            }
                            Collections.sort(lstIdGen);
                            PCommon.SaveClipboardIds(cmContext, lstIdGen);
                            GenerateTextForClipboard(true);
                        } catch (Exception ex) {
                            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
                        }
                    }

                    ///Returns text generated
                    String GenerateTextForClipboard(final boolean shouldShowToast) {
                        String textToClipboard = "";

                        try {
                            ArrayList<Integer> lstIdGen = PCommon.GetClipboardIds(cmContext);
                            final int size = lstIdGen.size();
                            if (size <= 0) {
                                ClearClipboard();
                                return "";
                            }

                            VerseBO currentVerse, nextVerse;
                            int idIntCurrent, idIntNext, bNumberCurrent, bNumberNext, cNumberCurrent, cNumberNext, vNumberCurrent;
                            String vTextCurrent, bNameCurrent, bNameNext;
                            int prevId = -1;

                            final StringBuilder sb = new StringBuilder();
                            for (int index = 0; index < size; index++) {
                                //Current
                                idIntCurrent = lstIdGen.get(index);
                                currentVerse = _s.GetVerse(idIntCurrent);
                                bNumberCurrent = currentVerse.bNumber;
                                cNumberCurrent = currentVerse.cNumber;
                                vNumberCurrent = currentVerse.vNumber;
                                vTextCurrent = currentVerse.vText;
                                bNameCurrent = currentVerse.bName;

                                if (prevId == -1) {
                                    sb.append(PCommon.ConcaT("\n\n", bNameCurrent, " ", cNumberCurrent, "\n"));
                                } else if (prevId + 1 != idIntCurrent) {
                                    sb.append("\n");
                                }
                                sb.append(PCommon.ConcaT(vNumberCurrent, ": ", vTextCurrent, "\n"));
                                prevId = idIntCurrent;

                                //Next
                                if ((index + 1) < size) {
                                    idIntNext = lstIdGen.get(index + 1);

                                    nextVerse = _s.GetVerse(idIntNext);
                                    bNumberNext = nextVerse.bNumber;
                                    cNumberNext = nextVerse.cNumber;
                                    bNameNext = nextVerse.bName;

                                    if ((bNumberCurrent != bNumberNext) || (cNumberCurrent != cNumberNext) || (bNameCurrent.compareTo(bNameNext) != 0)) {
                                        prevId = -1;
                                    }
                                }
                            }
                            textToClipboard = sb.toString().trim();
                            PCommon.CopyTextToClipboard(cmContext, "", textToClipboard, shouldShowToast);
                        } catch (Exception ex) {
                            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
                        }
                        return textToClipboard;
                    }
                }

                final InnerClass innerClass = new InnerClass();
                final String mnuCopyVerseToClipboard = getString(R.string.mnuAddVerseToClipboard);
                final String mnuCopyChapterToClipboard = getString(R.string.mnuAddChapterToClipboard);
                final String mnuCopyResultToClipboard = getString(R.string.mnuAddResultToClipboard);
                final String mnuClearClipboard = getString(R.string.mnuClipboardClear);
                final String mnuShareClipboard = getString(R.string.mnuShare);

                final ArrayList<String> mnuCopy = new ArrayList<>();
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE)
                    mnuCopy.add(mnuCopyVerseToClipboard);
                mnuCopy.add(mnuCopyChapterToClipboard);
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE)
                    mnuCopy.add(mnuCopyResultToClipboard);
                mnuCopy.add(mnuClearClipboard);
                mnuCopy.add(mnuShareClipboard);

                final AlertDialog builderCopyMenu = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                ShowMenu(builderCopyMenu, R.string.mnuClipboard, mnuCopy);
                builderCopyMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuShareClipboard) == 0) {
                            final String textToShare = innerClass.GenerateTextForClipboard(false);
                            PCommon.ShareText(getContext(), textToShare);
                        } else if (mnu_dialog.compareTo(mnuClearClipboard) == 0) {
                            innerClass.ClearClipboard();
                            builderCopyMenu.show();
                        } else if (mnu_dialog.compareTo(mnuCopyVerseToClipboard) == 0) {
                            innerClass.AddToClipboard(CLIPBOARD_ADD_TYPE.VERSE, null);
                        } else if (mnu_dialog.compareTo(mnuCopyChapterToClipboard) == 0) {
                            innerClass.AddToClipboard(CLIPBOARD_ADD_TYPE.CHAPTER, null);
                        } else if (mnu_dialog.compareTo(mnuCopyResultToClipboard) == 0) {
                            final int tabIdFrom = tabNumber();
                            if (tabIdFrom >= 0) {
                                if (isVerse) {
                                    innerClass.AddToClipboard(CLIPBOARD_ADD_TYPE.VERSE, null);
                                } else if (isChapter) {
                                    innerClass.AddToClipboard(CLIPBOARD_ADD_TYPE.CHAPTER, null);
                                } else {
                                    final CacheTabBO t = _s.GetCurrentCacheTab();
                                    if (t == null) return;

                                    final String searchString = t.fullQuery;
                                    ArrayList<Integer> lstAllId = new ArrayList<>();

                                    if (fragmentType == FRAGMENT_TYPE.FAV_TYPE) {
                                        //Fav
                                        final int favOrder = 0;
                                        final int favType = PCommon.GetFavFilter(getContext());
                                        final ArrayList<VerseBO> lstVerse = _s.SearchFav(verse.bbName, searchString, favOrder, favType);
                                        for (VerseBO verse : lstVerse) {
                                            lstAllId.add(verse.id);
                                        }
                                    } else {
                                        //CR
                                        final String[] words = searchString.split("\\s");
                                        final String patternDigit = "\\d+";
                                        if (words.length == 4 && (words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].equalsIgnoreCase("CR:"))) {
                                            final ArrayList<VerseBO> lstVerse = _s.GetCrossReferences(verse.bbName, t.bNumber, t.cNumber, t.vNumber);
                                            for (VerseBO verse : lstVerse) {
                                                lstAllId.add(verse.id);
                                            }
                                        } else {
                                            //Others
                                            final int tabIdTo = MainActivity.Tab.GetTabCount();
                                            if (tabIdTo < 0) return;
                                            lstAllId = _s.GetResultVersesId(tabIdFrom, tabIdTo, verse.bbName);
                                            _s.DeleteCache(tabIdTo);
                                        }
                                    }
                                    innerClass.AddToClipboard(CLIPBOARD_ADD_TYPE.ALL, lstAllId);
                                }
                            }
                        }
                    }
                });
            } else if (itemId == R.id.mnu_cr) {
                if (verse == null) return false;

                final AlertDialog builder = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                final LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
                final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, (ViewGroup) getActivity().findViewById(R.id.llLanguages));

                final String msg = PCommon.ConcaT(getString(R.string.mnuCrossReferences), "");
                PCommon.SelectBibleLanguageMulti(builder, getContext(), vllLanguages, msg, "", true, false);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final Context dlgContext = ((AlertDialog) dialogInterface).getContext();
                        final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                        if (bbname.equals("")) return;
                        final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber, " CR:");
                        final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);
                        MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, verse.vNumber, fullquery);
                    }
                });
                builder.show();
            } else if (itemId == R.id.mnu_fav) {
                if (verse == null) return false;

                final String mnuNoteFavAdd = getString(R.string.mnuNoteFavAdd);
                final String mnuNoteReadingAdd = getString(R.string.mnuNoteReadingAdd);
                final String mnuNoteRemove = getString(R.string.mnuNoteRemove);

                final ArrayList<String> mnuFav = new ArrayList<String>() {{
                    add(mnuNoteFavAdd);
                    add(mnuNoteReadingAdd);
                    add(mnuNoteRemove);
                }};

                final AlertDialog builderFavMenu = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                ShowMenu(builderFavMenu, R.string.mnuOpen, mnuFav);
                builderFavMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuNoteFavAdd) == 0) {
                            //TODO: mark is hardcoded! final int markType = 1;
                            final String changeDt = PCommon.NowYYYYMMDD();
                            final NoteBO noteBO = new NoteBO(verse.bNumber, verse.cNumber, verse.vNumber, changeDt, 1);
                            _s.SaveNote(noteBO);

                            UpdateViewMark(position, 1);

                            //return true;
                        } else if (mnu_dialog.compareTo(mnuNoteReadingAdd) == 0) {
                            //TODO: warning hardcoded
                            final String changeDt = PCommon.NowYYYYMMDD();
                            final NoteBO noteBO = new NoteBO(verse.bNumber, verse.cNumber, verse.vNumber, changeDt, 2);
                            _s.SaveNote(noteBO);

                            UpdateViewMark(position, 2);

                            //return true;
                        } else if (mnu_dialog.compareTo(mnuNoteRemove) == 0) {
                            _s.DeleteNote(verse.bNumber, verse.cNumber, verse.vNumber);

                            UpdateViewMark(position, 0);

                            //return true;
                        }
                    }
                });
            } else if (itemId == R.id.mnu_listen) {
                final String listenPosCurrentChapterTitle = verse == null ? null : PCommon.ConcaT(verse.bName, " ", verse.cNumber);
                final String listCurrentChapterTitle = listenPosCurrentChapterTitle == null
                        ? getString(R.string.mnuListenCurrentChapter)
                        : PCommon.ConcaT(getString(R.string.mnuListenCurrentChapter),
                        " (",
                        listenPosCurrentChapterTitle,
                        ")");
                final int listenStatus = PCommon.GetListenStatus(v.getContext());
                final boolean listen_cmd_visibility = listenStatus > 0;

                final String mnuListenStopConfirm = getString(R.string.mnuListenStopConfirm);
                final String mnuListenReplay = getString(R.string.mnuListenReplay);
                final String mnuListenNextChapter = getString(R.string.mnuListenNextChapter);
                final String mnuListenPreviousChapter = getString(R.string.mnuListenPreviousChapter);
                final String mnuListenCurrentChapter = listCurrentChapterTitle;

                final ArrayList<String> mnuListen = new ArrayList<>();
                if (listen_cmd_visibility) {
                    mnuListen.add(mnuListenStopConfirm);
                } else {
                    mnuListen.add(mnuListenReplay);
                    mnuListen.add(mnuListenNextChapter);
                    mnuListen.add(mnuListenPreviousChapter);
                    if (verse != null) mnuListen.add(mnuListenCurrentChapter);
                }

                final AlertDialog builderOpenMenu = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                final String mnuTitle = item.getTitle().toString();
                ShowMenu(builderOpenMenu, mnuTitle, mnuListen);
                builderOpenMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuListenStopConfirm) == 0) {
                            _s.SayStop();
                        } else if (mnu_dialog.compareTo(mnuListenReplay) == 0 ||
                                mnu_dialog.compareTo(mnuListenPreviousChapter) == 0 ||
                                mnu_dialog.compareTo(mnuListenNextChapter) == 0) {
                            final String[] arrListen = _s.GetListenPosition(getContext());
                            if (arrListen != null && arrListen.length >= 4) {
                                final String listen_bbname = arrListen[0];
                                final int listen_bnumber = Integer.parseInt(arrListen[1]);
                                final int listen_cnumber = Integer.parseInt(arrListen[2]);

                                if (mnu_dialog.compareTo(mnuListenReplay) == 0) {
                                    final int vNumber = 1;
                                    _s.Say(listen_bbname, listen_bnumber, listen_cnumber, vNumber);
                                } else if (mnu_dialog.compareTo(mnuListenPreviousChapter) == 0) {
                                    if ((listen_cnumber - 1) < 1) {
                                        PCommon.ShowToast(getContext(), R.string.toastChapterFailure, Toast.LENGTH_SHORT);
                                    } else {
                                        final int vNumber = 1;
                                        _s.Say(listen_bbname, listen_bnumber, listen_cnumber - 1, vNumber);
                                    }
                                } else {
                                    //Next chapter
                                    final int chapterMax = _s.GetBookChapterMax(listen_bnumber);
                                    if ((listen_cnumber + 1) > chapterMax) {
                                        PCommon.ShowToast(getContext(), R.string.toastChapterFailure, Toast.LENGTH_SHORT);
                                    } else {
                                        final int vNumber = 1;
                                        _s.Say(listen_bbname, listen_bnumber, listen_cnumber + 1, vNumber);
                                    }
                                }
                            }
                        } else if (mnu_dialog.compareTo(mnuListenCurrentChapter) == 0) {
                            if (verse == null) return;

                            final String mnuListenCurrentChapterFrom1 = getString(R.string.mnuListenCurrentChapterFrom1);
                            final String mnuListenCurrentChapterFromPos = getString(R.string.mnuListenCurrentChapterFromPos);
                            final ArrayList<String> mnuListenCurrentChapter = new ArrayList<String>() {{
                                add(mnuListenCurrentChapterFrom1);
                                add(mnuListenCurrentChapterFromPos);
                            }};

                            final AlertDialog builderListenCurrentChapterMenu = new AlertDialog.Builder(Objects.requireNonNull(Objects.requireNonNull(getContext()))).create();
                            ShowMenu(builderListenCurrentChapterMenu, R.string.mnuListenCurrentChapter, mnuListenCurrentChapter);
                            builderListenCurrentChapterMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                                    if (mnu_dialog.compareTo(mnuListenCurrentChapterFrom1) == 0 ||
                                            mnu_dialog.compareTo(mnuListenCurrentChapterFromPos) == 0) {
                                        final int vNumber = mnu_dialog.compareTo(mnuListenCurrentChapterFrom1) == 0 ? 1 : verse.vNumber;
                                        _s.Say(verse.bbName, verse.bNumber, verse.cNumber, vNumber);
                                    }
                                }
                            });
                        }
                    }
                });
            } else if (itemId == R.id.mnu_edit) {
                final int edit_status = PCommon.GetEditStatus(getContext());
                if (edit_status == 1) {
                    //Stop
                    PCommon.SavePrefInt(getContext(), IProject.APP_PREF_KEY.EDIT_STATUS, 0);
                    PCommon.SavePrefInt(getContext(), IProject.APP_PREF_KEY.EDIT_ART_ID, -1);
                    PCommon.SavePref(getContext(), IProject.APP_PREF_KEY.EDIT_SELECTION, "");
                } else {
                    //Selection Start
                    PCommon.ShowArticles(getContext(), true, true);
                }

                return true;
            } else if (itemId == R.id.mnu_edit_select_from) {
                if (verse == null) return false;

                final int artId = PCommon.GetEditArticleId(getContext());
                if (artId < 0) return false;

                final String selectFrom = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                PCommon.SavePref(getContext(), IProject.APP_PREF_KEY.EDIT_SELECTION, selectFrom);

                return true;
            } else if (itemId == R.id.mnu_edit_select_to) {
                if (verse == null) return false;

                final int artId = PCommon.GetEditArticleId(getContext());
                if (artId < 0) return false;

                final String selectFrom = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.EDIT_SELECTION, "");
                if (selectFrom.isEmpty()) {
                    PCommon.ShowToast(getContext(), R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }
                final String selectTo = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                final String[] arrFrom = selectFrom.split("\\s");
                final String[] arrTo = selectTo.split("\\s");

                if (arrFrom.length != 3 || arrTo.length != 3) {
                    PCommon.ShowToast(getContext(), R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }
                if (arrFrom[0].compareTo(arrTo[0]) != 0 || arrFrom[1].compareTo(arrTo[1]) != 0) {
                    PCommon.ShowToast(getContext(), R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }
                if (Integer.parseInt(arrFrom[2]) > Integer.parseInt(arrTo[2])) {
                    PCommon.ShowToast(getContext(), R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }

                PCommon.SavePref(getContext(), IProject.APP_PREF_KEY.EDIT_SELECTION, "");

                final String ref = PCommon.ConcaT("<R>", arrFrom[0], " ", arrFrom[1], " ", arrFrom[2], " ", arrTo[2], "</R>");
                final String source = _s.GetMyArticleSource(artId);
                final String finalSource = PCommon.ConcaT(source, ref);
                _s.UpdateMyArticleSource(artId, finalSource);
                final String toast = PCommon.ConcaT(arrFrom[1], ".", arrFrom[2], "-", arrTo[2], " ", getString(R.string.toastRefAdded));
                PCommon.ShowToast(getContext(), toast, Toast.LENGTH_SHORT);

                return true;
            } else if (itemId == R.id.mnu_edit_select_from_to) {
                if (verse == null) return false;

                final int artId = PCommon.GetEditArticleId(getContext());
                if (artId < 0) return false;

                final String selectFromTo = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber, " ", verse.vNumber);

                PCommon.SavePref(getContext(), IProject.APP_PREF_KEY.EDIT_SELECTION, "");

                final String ref = PCommon.ConcaT("<R>", selectFromTo, "</R>");
                final String source = _s.GetMyArticleSource(artId);
                final String finalSource = PCommon.ConcaT(source, ref);
                _s.UpdateMyArticleSource(artId, finalSource);
                final String toast = PCommon.ConcaT(verse.cNumber, ".", verse.vNumber, " ", getString(R.string.toastRefAdded));
                PCommon.ShowToast(getContext(), toast, Toast.LENGTH_SHORT);

                return true;
            } else if (itemId == R.id.mnu_edit_move) {
                final String mnuEditMoveUp = getString(R.string.mnuEditMoveUp);
                final String mnuEditMoveDown = getString(R.string.mnuEditMoveDown);

                final ArrayList<String> mnuEditMove = new ArrayList<String>() {{
                    add(mnuEditMoveUp);
                    add(mnuEditMoveDown);
                }};

                final AlertDialog builderEditMenu = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                ShowMenu(builderEditMenu, R.string.mnuEditMove, mnuEditMove);
                builderEditMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuEditMoveUp) == 0) {
                            final int artId = Integer.parseInt(tabTitle.replace(getString(R.string.tabMyArtPrefix), ""));
                            if (artId >= 0) {
                                final String source = MoveArticleShortSection(position, -1);
                                if (source != null) {
                                    _s.UpdateMyArticleSource(artId, source);
                                    onResume();
                                }

                                //return true;
                            }
                        } else if (mnu_dialog.compareTo(mnuEditMoveDown) == 0) {
                            final int artId = Integer.parseInt(tabTitle.replace(getString(R.string.tabMyArtPrefix), ""));
                            if (artId >= 0) {
                                final String source = MoveArticleShortSection(position, +1);
                                if (source != null) {
                                    _s.UpdateMyArticleSource(artId, source);
                                    onResume();
                                }

                                //return true;
                            }
                        }
                    }
                });
            } else if (itemId == R.id.mnu_edit_add) {
                final String mnuEditAddText = getString(R.string.mnuEditAddText);
                final String mnuEditAddTitleSmall = getString(R.string.mnuEditAddTitleSmall);
                final String mnuEditAddTitleLarge = getString(R.string.mnuEditAddTitleLarge);

                final ArrayList<String> mnuEditAdd = new ArrayList<String>() {{
                    add(mnuEditAddText);
                    add(mnuEditAddTitleSmall);
                    add(mnuEditAddTitleLarge);
                }};

                final AlertDialog builderEditAdd = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                ShowMenu(builderEditAdd, R.string.mnuEditAdd, mnuEditAdd);
                builderEditAdd.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuEditAddText) == 0 ||
                                mnu_dialog.compareTo(mnuEditAddTitleSmall) == 0 ||
                                mnu_dialog.compareTo(mnuEditAddTitleLarge) == 0) {
                            final int artId = Integer.parseInt(tabTitle.replace(getString(R.string.tabMyArtPrefix), ""));
                            if (artId >= 0) {
                                final String editType = mnu_dialog.compareTo(mnuEditAddText) == 0 ? "T"
                                        : mnu_dialog.compareTo(mnuEditAddTitleLarge) == 0 ? "L" : "S";

                                EditArticleDialog(false, editType, getActivity(), R.string.mnuEditAdd, "", position, artId);

                                //return true;
                            }
                        }
                    }
                });
            } else if (itemId == R.id.mnu_edit_update) {
                final int artId = Integer.parseInt(tabTitle.replace(getString(R.string.tabMyArtPrefix), ""));
                if (artId < 0) return false;

                final ShortSectionBO updateSection = FindArticleShortSectionByPositionId(position);
                final String updateSectionContent = (updateSection == null) ? "" : updateSection.content;
                EditArticleDialog(true, "T", getActivity(), R.string.mnuEditUpdate, updateSectionContent, position, artId);

                return true;
            } else if (itemId == R.id.mnu_edit_remove) {
                final String mnuEditRemoveConfirm = getString(R.string.mnuEditRemoveConfirm);
                final ArrayList<String> mnuEditRemove = new ArrayList<String>() {{
                    add(mnuEditRemoveConfirm);
                }};

                final AlertDialog builderEditRemove = new AlertDialog.Builder(Objects.requireNonNull(getContext())).create();
                ShowMenu(builderEditRemove, R.string.mnuEditRemove, mnuEditRemove);
                builderEditRemove.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuEditRemoveConfirm) == 0) {
                            final int artId = Integer.parseInt(tabTitle.replace(getString(R.string.tabMyArtPrefix), ""));
                            if (artId >= 0) {
                                final String source = DeleteArticleShortSection(position);
                                if (source != null) {
                                    _s.UpdateMyArticleSource(artId, source);
                                    onResume();
                                }

                                //return true;
                            }
                        }
                    }
                });
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return super.onContextItemSelected(item);
    }

    /***
     * Show custom menu
     * @param builder       Builder menu
     * @param mnuTitle      Menu title
     * @param lstMnuItem    List of menu items
     * return Set MENU_DIALOG
     */
    private void ShowMenu(final AlertDialog builder, final String mnuTitle, final ArrayList<String> lstMnuItem)
    {
        final Context context = builder.getContext();

        try
        {
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context) + 2;

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            //final LinearLayout llMnu = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.context_menu, (ViewGroup) getActivity().findViewById(R.id.llMnu));
            //TextView tvMnuItemGen = llMnu.findViewById(R.id.tv_menu_item);

            final LinearLayout llMnu = new LinearLayout(context);
            llMnu.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llMnu.setOrientation(LinearLayout.VERTICAL);
            llMnu.setPadding(15, 15, 15, 15);

            for (final String ref : lstMnuItem)
            {
                final TextView tvMnuItem = new TextView(context);
                tvMnuItem.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvMnuItem.setPadding(10, 15, 10, 15);
                tvMnuItem.setText( ref );
                tvMnuItem.setTag( ref );
                tvMnuItem.setOnClickListener(v -> {
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, (String) v.getTag());
                    builder.dismiss();
                });

                //TODO FAB: slow GetDrawable
                tvMnuItem.setFocusable(true);
                tvMnuItem.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) { tvMnuItem.setTypeface(typeface); }
                tvMnuItem.setTextSize(fontSize);

                llMnu.addView(tvMnuItem);
            }
            sv.addView(llMnu);

            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    PCommon.SavePref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                }
            });
            builder.setTitle(mnuTitle);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show custom menu
     * @param builder       Builder menu
     * @param mnuTitleId    Menu title
     * @param lstMnuItem    List of menu items
     * return Set MENU_DIALOG
     */
    private void ShowMenu(final AlertDialog builder, final int mnuTitleId, final ArrayList<String> lstMnuItem)
    {
        try
        {
            final String mnuTitle = getString(mnuTitleId);

            ShowMenu(builder, mnuTitle, lstMnuItem);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull final Menu menu, @NonNull MenuInflater menuInflater)
    {
        try
        {
            super.onCreateOptionsMenu(menu, menuInflater);

            SetLocalBibleName();

            final MenuItem mnu_group_settings = menu.findItem(R.id.mnu_group_settings);
            if (mnu_group_settings.isEnabled())
            {
                final int settingsDrawable = !MainActivity.Tab.IsThemeWhite() ? R.drawable.ic_settings_white_24dp : R.drawable.ic_settings_black_24dp;
                PCommon.SetIconMenuItem(_context, mnu_group_settings, settingsDrawable);
            }

            if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
            {
                menuInflater.inflate(R.menu.menu_search, menu);
            }
            else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
            {
                menuInflater.inflate(R.menu.menu_plan, menu);
            }
            else if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                menuInflater.inflate(R.menu.menu_fav, menu);

                //No search
                return;
            }
            else
            {
                //Article
                final int INSTALL_STATUS = PCommon.GetInstallStatus(_context);
                if (INSTALL_STATUS == 6) menuInflater.inflate(R.menu.menu_art, menu);

                //No search
                return;
            }

            final MenuItem mnu_search_item = menu.findItem(R.id.mnu_search);
            searchView = (SearchView) mnu_search_item.getActionView();
            searchView.setLongClickable(true);
            searchView.setIconifiedByDefault(true);
            searchView.setQueryHint((fragmentType == FRAGMENT_TYPE.SEARCH_TYPE) ? getString(R.string.searchBibleHint) : getString(R.string.searchFavHint));

            if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
            {
                searchView.setOnSearchClickListener(v -> searchView.setQuery(searchFullQuery, false));

                final MenuItem mnu_search_prbl = menu.findItem(R.id.mnu_search_prbl);
                final MenuItem mnu_search_bible_name_item = menu.findItem(R.id.mnu_search_bible_name);
                final String bbNameLanguage = (bbName.compareToIgnoreCase("k") == 0) ? "EN" : (bbName.compareToIgnoreCase("d") == 0) ? "IT" : (bbName.compareToIgnoreCase("v") == 0) ? "ES" : (bbName.compareToIgnoreCase("l") == 0) ? "FR-LSG" : (bbName.compareToIgnoreCase("a") == 0) ? "PT" : "FR-OST";
                mnu_search_bible_name_item.setTitle(bbNameLanguage);
                mnu_search_bible_name_item.setVisible(false);
                mnu_search_prbl.setVisible(isPrbl);

                final String bibleAppType = PCommon.GetPrefBibleAppType(_context);
                final boolean showBibleNameItem = bibleAppType.compareTo("M") == 0;

                final MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        mnu_search_bible_name_item.setVisible(false);
                        mnu_search_prbl.setVisible(isPrbl);
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        mnu_search_prbl.setVisible(false);
                        if (showBibleNameItem) {
                            mnu_search_bible_name_item.setVisible(true);
                        } else {
                            bbName = PCommon.GetPrefBibleName(_context);
                        }
                        return true;
                    }
                };
                mnu_search_item.setOnActionExpandListener(expandListener);

                searchView.setSuggestionsAdapter(cursorAdapter);
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
                {
                    @Override
                    public boolean onQueryTextSubmit(String query)
                    {
                        if (PCommon._isDebugVersion) System.out.println("TextSubmit: " + query);

                        //Save
                        searchFullQuery = query;
                        trad = bbName;

                        SearchBible(false);

                        mnu_search_item.collapseActionView();

                        isPrbl = GetPrblName(searchFullQuery) != null;
                        mnu_search_prbl.setVisible(isPrbl);

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query)
                    {
                        if (PCommon._isDebugVersion) System.out.println("TextChange: " + query);

                        return false;
                    }
                });
                searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener()
                {
                    @Override
                    public boolean onSuggestionSelect(int position)
                    {
                        return false;
                    }

                    @Override
                    public boolean onSuggestionClick(int position)
                    {
                        if (PCommon._isDebugVersion) System.out.println("Suggestion Click: pos=" + position);

                        //Get selected book
                        @SuppressWarnings("unused") final int bookId = matrixCursor.getInt(0);
                        final String bookName = matrixCursor.getString(1);

                        if (PCommon._isDebugVersion) System.out.println("Book (" + bookId + ") => " + bookName);

                        //Change SearchView query
                        searchView.setQuery(bookName, false);

                        return true;
                    }
                });

                final AutoCompleteTextView searchViewAutoComplete = searchView.findViewById(R.id.search_src_text);
                searchViewAutoComplete.setThreshold(1);
            }
            else
            {
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
                {
                    @Override
                    public boolean onQueryTextSubmit(String query)
                    {
                        if (PCommon._isDebugVersion) System.out.println("TextSubmit: " + query);

                        //Save
                        searchFullQuery = query;

                        SearchBible(false);
                        mnu_search_item.collapseActionView();

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query)
                    {
                        if (PCommon._isDebugVersion) System.out.println("TextChange: " + query);

                        return false;
                    }
                });
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {
        final int id = item.getItemId();
        if (id == R.id.mnu_search_bible_name) {
            SetLocalBibleName();

            bbName = RollBookName(bbName);

            final String bbNameLanguage = (bbName.compareToIgnoreCase("k") == 0) ? "EN" : (bbName.compareToIgnoreCase("d") == 0) ? "IT" : (bbName.compareToIgnoreCase("v") == 0) ? "ES" : (bbName.compareToIgnoreCase("l") == 0) ? "FR-LSG" : (bbName.compareToIgnoreCase("a") == 0) ? "PT" : "FR-OST";
            item.setTitle(bbNameLanguage);

            return true;
        } else if (id == R.id.mnu_fav_search) {
            try {
                ((MainActivity) Objects.requireNonNull(getActivity())).SearchDialog(getContext(), false);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
            }

            return true;
        } else if (id == R.id.mnu_fav_clear_filter) {//Clear Fav filter
            searchFullQuery = "";
            final int orderBy = PCommon.GetFavOrder(getContext());

            PCommon.SavePrefInt(getContext(), IProject.APP_PREF_KEY.FAV_FILTER, 0);
            SaveTab();

            recyclerViewAdapter = new BibleAdapter(getContext(), bbName, searchFullQuery, orderBy, 0);
            if (WhenTabIsEmptyOrNull(true)) return true;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(scrollPosY);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance()
    {
        if (_context == null) _context = Objects.requireNonNull(getActivity()).getApplicationContext();

        CheckLocalInstance(_context);
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null)
            {
                _s = SCommon.GetInstance(context);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    private void CreateRecyclerView()
    {
        SetLayoutManager();

        recyclerViewAdapter = new BibleAdapter();
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setHasFixedSize(true);
    }

    private void SetLayoutManager()
    {
        recyclerView = v.findViewById(R.id.card_recycler_view);

        final RecyclerView.LayoutManager layoutManager;
        if (trad == null || fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE || fragmentType == FRAGMENT_TYPE.FAV_TYPE)
        {
            layoutManager = new LinearLayoutManager(_context);
        }
        else
        {
            final int ic = trad.length();
            final int dc = this.GetDynamicColumnCount(ic);
            layoutManager = (dc <= 1) ? new LinearLayoutManager(_context) : new GridLayoutManager(_context, dc);
        }
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        registerForContextMenu(recyclerView);

        if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE) {
            recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(_context, recyclerView, new RecyclerViewTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                }

                @Override
                public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float X, float Y) {
                    if (isChapterListLoading) return false;

                    if ((motionEvent1.getX() - motionEvent2.getX() > 150) && (motionEvent2.getY() - motionEvent1.getY() < 20)) {
                        ShowChapterList(v.getContext());
                        return true;
                    }
                    return false;
                }
            }));
        }
    }

    /***
     * Get article
     * @param t     CacheTabBO
     */
    private ArtOriginalContentBO GetArticle(final CacheTabBO t)
    {
        if (t == null) return null;

        ArtOriginalContentBO artOriginalContent = null;

        try
        {
            final int artId;
            final String artTitle;
            final String artHtml;
            final String ha;

            if (t.fullQuery.startsWith("ART"))
            {
                artId = PCommon.GetResId(_context, PCommon.ConcaT(t.fullQuery, "_CONTENT"));
                artHtml = _context.getString(artId);
                artTitle = _context.getString(PCommon.GetResId(_context, t.fullQuery));
                ha = PCommon.ConcaT("<br><H>", artTitle, "</H>");
            }
            else
            {
                artId = Integer.parseInt(t.fullQuery);
                artHtml = _s.GetMyArticleSource(artId);
                ha = null;
            }
            artOriginalContent = new ArtOriginalContentBO(ha, artHtml);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return artOriginalContent;
    }

    private Cursor GetCursor(final CharSequence query)
    {
        if (query == null) return null;

        final MatrixCursor cursor = new MatrixCursor(new String[]{"_id", "text"});

        try
        {
            final ArrayList<BibleRefBO> lstRef = _s.GetListBookByName(bbName, query.toString());

            if (lstRef.size() > 0)
            {
                String bookId;
                String bookName;

                for (final BibleRefBO r : lstRef)
                {
                    bookId = Integer.toString(r.bNumber);
                    bookName = r.bName;

                    cursor.addRow(new String[]{bookId, bookName});
                }
            }

            matrixCursor = cursor;
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return cursor;
    }

    private boolean IsChapterExist(final String bbName, final int bNumber, final int cNumber)
    {
        final ArrayList<VerseBO> lstVerse = _s.GetVerse(bbName, bNumber, cNumber, 1);

        return !(lstVerse == null || lstVerse.size() == 0);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean IsVerseExist(final String bbName, final int bNumber, final int cNumber, final int vNumber)
    {
        final ArrayList<VerseBO> lstVerse = _s.GetVerse(bbName, bNumber, cNumber, vNumber);

        return !(lstVerse == null || lstVerse.size() == 0);
    }

    /***
     * Update view at position and show/hide the mark
     * @param position  Position
     * @param markType  Mark type
     */
    private void UpdateViewMark(final int position, final int markType)
    {
        try
        {
            if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                SearchBible(false);
                return;
            }

            ((BibleAdapter) recyclerViewAdapter).lstVerse.get(position).mark = markType;
            recyclerViewAdapter.notifyItemChanged(position);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    private void ShowArticle(final int step)
    {
        try
        {
            if (!(fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE && searchFullQuery.startsWith("ART"))) return;

            //Get Art
            final String[] arr = _context.getResources().getStringArray(R.array.ART_ARRAY);
            final List<String> lstArt = Arrays.asList(arr);
            final int idxArt = lstArt.indexOf(searchFullQuery);
            if (idxArt <= 0 && step < 0) return;
            if (idxArt == lstArt.size() && step > 0) return;

            //Update Cache
            final String tabTitle;
            final String updSearchFullQuery = lstArt.get(idxArt + step);
            if (updSearchFullQuery.startsWith("ART_APP_")) {
                final int artId = PCommon.GetResId(_context, updSearchFullQuery);
                tabTitle = _context.getString(artId);
            } else {
                tabTitle = updSearchFullQuery;
            }

            final int tabId = tabNumber();
            final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
            cacheTab.isBook = false;
            cacheTab.isChapter = false;
            cacheTab.isVerse = false;
            cacheTab.bNumber = 0;
            cacheTab.cNumber = 0;
            cacheTab.vNumber = 0;
            cacheTab.fullQuery = updSearchFullQuery;
            cacheTab.scrollPosY = 0;
            cacheTab.tabTitle = tabTitle;
            _s.SaveCacheTab(cacheTab);

            onResume();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    /**
     * Get prbl name or null if not found
     * @param searchFullQuery   Query
     * @return null if not found
     */
    private String GetPrblName(final String searchFullQuery)
    {
        try
        {
            final String[] words = searchFullQuery.split("\\s");
            final String patternDigit = "\\d+";

            if (words.length != 4) return null;
            if (words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].matches(patternDigit))
            {
                final String[] arr = _context.getResources().getStringArray(R.array.PRBL_ARRAY);
                final String updQuery = PCommon.ConcaT("|", searchFullQuery);
                for (String prbl : arr) {
                    if (prbl.contains(updQuery)) return prbl.split("\\|")[0];
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return null;
    }

    private void ShowPrbl(final int step)
    {
        try
        {
            //Get Prbl
            int idxPrbl = -1;
            int idxTmp = -1;
            final String[] arr = _context.getResources().getStringArray(R.array.PRBL_ARRAY);
            final List<String> lstPrbl = Arrays.asList(arr);
            final String updQuery = PCommon.ConcaT("|", searchFullQuery);
            for (String prbl : lstPrbl) {
                idxTmp++;
                if (prbl.contains(updQuery)) {
                    idxPrbl = idxTmp;
                    break;
                }
            }
            if (idxPrbl <= 0 && step < 0) return;
            if (idxPrbl == lstPrbl.size() && step > 0) return;

            //Update Cache
            final String[] resPrbl = lstPrbl.get(idxPrbl + step).split("\\|");
            final int tabId = tabNumber();
            final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
            cacheTab.isBook = false;
            cacheTab.isChapter = false;
            cacheTab.isVerse = false;
            cacheTab.bNumber = 0;
            cacheTab.cNumber = 0;
            cacheTab.vNumber = 0;
            cacheTab.fullQuery = resPrbl[1];
            cacheTab.scrollPosY = 0;
            cacheTab.tabTitle = resPrbl[0];
            _s.SaveCacheTab(cacheTab);

            onResume();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    private void ShowChapter(final String bbName, final int bNumber, final int cNumber)
    {
        try
        {
            //Try
            if (!IsChapterExist(bbName, bNumber, cNumber)) return;

            //Book title
            ShowBookTitle(true);

            final BibleRefBO ref = _s.GetBookRef(bbName, bNumber);
            tvBookTitle.setText(ref.bName.toUpperCase());

            final String title = PCommon.ConcaT(ref.bsName, cNumber);
            SetTabTitle(title);
            SaveTab();

            //Get chapter
            recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(scrollPosY);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    @SuppressLint("DefaultLocale")
    private void ShowChapterList(final Context context) {
        try
        {
            isChapterListLoading = true;

            CheckLocalInstance(context);

            final int count = recyclerViewAdapter.getItemCount();
            if (count <= 0) {
                isChapterListLoading = false;
                return;
            }

            final ArrayList<VerseBO> arrVerse = ((BibleAdapter) recyclerViewAdapter).lstVerse;
            final SortedMap<String, String> mapChapter = new TreeMap();
            final String bbname = PCommon.GetPrefBibleName(context);

            VerseBO verse;
            String k, v;
            BibleRefBO r;
            for (int i = 0; i < count; i++) {
                verse = arrVerse.get(i);
                k = String.format("%2d-%3d", verse.bNumber, verse.cNumber).replaceAll(" ", "0");
                if (!mapChapter.containsKey(k)) {
                    r =_s.GetBookRef(bbname, verse.bNumber);
                    v = PCommon.ConcaT(verse.bNumber, "|", r.bName, "|", verse.cNumber, "|", i);
                    mapChapter.put(k, v);
                }
            }

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View svChapter = inflater.inflate(R.layout.fragment_chapter_list, (ViewGroup) Objects.requireNonNull(getActivity()).findViewById(R.id.glChapter));
            final GridLayout glChapter = svChapter.findViewById(R.id.glChapter);

            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuContent);
            builder.setView(svChapter);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    isChapterListLoading = false;
                }
            });
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    isChapterListLoading = false;
                }
            });

            int prevcbNumber = 0, prevccNumber = 0;
            int cbNumber, ccNumber, cPos;
            String cbName;
            final Spanned nbsp = Html.fromHtml("&nbsp;");
            TextView tvChapter = null, tvPref;
            HorizontalScrollView hsv = null;
            LinearLayout llChapter = null;
            Button btnChapter;

            for (String K : mapChapter.keySet()) {
                final String[] cValue = Objects.requireNonNull(mapChapter.get(K)).split("\\|");
                cbNumber = Integer.parseInt(cValue[0]);
                cbName = cValue[1];
                ccNumber = Integer.parseInt(cValue[2]);
                cPos = Integer.parseInt(cValue[3]);

                if (cbNumber > prevcbNumber)
                {
                    tvChapter = new TextView(context);
                    tvChapter.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                    tvChapter.setPadding(0, 20, 10, 0);
                    tvChapter.setFocusable(true);
                    tvChapter.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                    tvChapter.setText(PCommon.ConcaT(cbNumber, nbsp, cbName));
                    tvChapter.setTag(cPos);
                    if (typeface != null) tvChapter.setTypeface(typeface);
                    tvChapter.setTextSize(fontSize);
                    tvChapter.setOnClickListener(v1 -> {
                        builder.dismiss();
                        final int pos = (int) v1.getTag();
                        recyclerView.scrollToPosition(pos);
                    });

                    tvPref = new TextView(context);
                    tvPref.setLayoutParams(PCommon._layoutParamsWrap);
                    tvPref.setPadding(16,0,0,0);
                    tvPref.setText("");

                    llChapter = new LinearLayout(context);
                    llChapter.setLayoutParams(PCommon._layoutParamsWrap);
                    llChapter.setOrientation(LinearLayout.HORIZONTAL);
                    llChapter.addView(tvPref);

                    hsv = new HorizontalScrollView(context);
                    hsv.setLayoutParams(PCommon._layoutParamsWrap);
                    hsv.addView(llChapter);

                    glChapter.addView(tvChapter);
                    glChapter.addView(hsv);

                    prevccNumber = 0;
                }

                if (ccNumber > prevccNumber)
                {
                    btnChapter = new Button(context);
                    btnChapter.setLayoutParams(PCommon._layoutParamsWrap);
                    btnChapter.setFocusable(true);
                    btnChapter.setText(String.valueOf(ccNumber));
                    btnChapter.setTag(cPos);
                    btnChapter.setOnClickListener(v12 -> {
                        builder.dismiss();
                        final int pos = (int) v12.getTag();
                        recyclerView.scrollToPosition(pos);
                    });
                    if (llChapter != null) llChapter.addView(btnChapter);
                }

                prevcbNumber = cbNumber;
                prevccNumber = ccNumber;
            }

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show plan
     * @param planDayMove   Move number of days in plan calendar
     */
    private void ShowPlan(final int planDayMove)
    {
        try
        {
            //Check
            final PlanDescBO pd = _s.GetPlanDesc(planId);
            if (pd == null) return;

            final int newPlanDayNumber = planDayNumber + planDayMove;
            if (newPlanDayNumber > pd.dayCount)
            {
                final Button btnForward = v.findViewById(R.id.btnForward);
                btnForward.setEnabled(false);
                return;
            }
            if (newPlanDayNumber < 1)
            {
                final Button btnBack = v.findViewById(R.id.btnBack);
                btnBack.setEnabled(false);
                return;
            }

            //Save
            final CacheTabBO t = _s.GetCacheTab(tabNumber());
            if (t == null) return;
            t.fullQuery = PCommon.ConcaT(planId, " ", newPlanDayNumber, " ", planPageNumber);
            t.scrollPosY = 0;
            _s.SaveCacheTab(t);

            final PlanCalBO pc =_s.GetPlanCalByDay(t.bbName, planId, newPlanDayNumber);
            final int bNumberStart = pc.bNumberStart, cNumberStart = pc.cNumberStart, vNumberStart = pc.vNumberStart;
            final int bNumberEnd = pc.bNumberEnd, cNumberEnd = pc.cNumberEnd, vNumberEnd = pc.vNumberEnd;
            final boolean copy =_s.CopyCacheSearchForOtherBible(t.tabNumber, t.trad, bNumberStart, cNumberStart, vNumberStart, bNumberEnd, cNumberEnd, vNumberEnd);
            if (!copy) return;

            onResume();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    private void ShowVerse(final String bbName, final int bNumber, final int cNumber, final int vNumber)
    {
        try
        {
            //Try
            if (!IsVerseExist(bbName, bNumber, cNumber, vNumber)) return;

            //Book title
            ShowBookTitle(false);

            final BibleRefBO ref = _s.GetBookRef(bbName, bNumber);
            tvBookTitle.setText(ref.bName.toUpperCase());

            final String title = PCommon.ConcaT(ref.bsName, cNumber, " ", vNumber);
            SetTabTitle(title);
            SaveTab();

            //Get verse
            recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber, vNumber);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(0);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    private void ShowVerses(final String bbName, final int bNumber, final int cNumber, final int vNumber, final int vNumberTo)
    {
        try
        {
            //Try
            if (!IsVerseExist(bbName, bNumber, cNumber, vNumber)) return;

            //Book title
            final String prblName = GetPrblName(searchFullQuery);
            isPrbl = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && prblName != null;
            ShowBookTitle(isPrbl);

            String tabTitle;
            if (prblName == null)
            {
                final BibleRefBO ref = _s.GetBookRef(bbName, bNumber);
                tvBookTitle.setText(ref.bName.toUpperCase());
                tabTitle = PCommon.ConcaT(ref.bsName, cNumber, " ", vNumber, "-", vNumberTo);
            }
            else
            {
                final int prblId = PCommon.GetResId(_context, prblName);
                tvBookTitle.setText(_context.getString(prblId).toUpperCase());
                tabTitle = prblName;
            }

            SetTabTitle(tabTitle);
            SaveTab();

            //Get verses
            recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber, vNumber, vNumberTo);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(scrollPosY);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    private void ShowBookTitle(final boolean show)
    {
        ShowBookTitle(show,null);
    }

    private void ShowBookTitle(final boolean show, final PlanCalBO pc) {
        try
        {
            tvBookTitle.setVisibility(show ? View.VISIBLE : View.GONE);
            btnBack.setVisibility(show ? View.VISIBLE : View.GONE);
            btnForward.setVisibility(show ? View.VISIBLE : View.GONE);

            if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE && pc != null) {
                final int checkDrawable = !MainActivity.Tab.IsThemeWhite() ? R.drawable.ic_check_white_24dp : R.drawable.ic_check_black_24dp;
                final int uncheckDrawable = !MainActivity.Tab.IsThemeWhite() ? R.drawable.ic_uncheck_white_24dp : R.drawable.ic_uncheck_black_24dp;
                final boolean isReadOrig = pc.isRead == 1;
                PCommon.SetIconTextView(_context, tvBookTitle, isReadOrig ? checkDrawable : uncheckDrawable);
                tvBookTitle.setTag(R.id.tv1, planId);
                tvBookTitle.setTag(R.id.tv2, planDayNumber);
                tvBookTitle.setOnClickListener(v -> {
                    final int planId = (int) v.getTag(R.id.tv1);
                    final int dayNumber = (int) v.getTag(R.id.tv2);
                    final PlanCalBO pc1 = _s.GetPlanCalByDay(bbName, planId, dayNumber);
                    if (pc1 == null) return;

                    final boolean isRead = pc1.isRead == 1;
                    final int updIsRead = isRead ? 0 : 1;
                    PCommon.SetIconTextView(v.getContext(), tvBookTitle, updIsRead == 1 ? checkDrawable : uncheckDrawable);

                    _s.MarkPlanCal(planId, planDayNumber, updIsRead);
                });
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    private void SaveRef(final boolean isBook, final boolean isChapter, final boolean isVerse, final int bNumber, final int cNumber, final int vNumber)
    {
        this.isBook = isBook;
        this.isChapter = isChapter;
        this.isVerse = isVerse;
        this.bNumber = bNumber;
        this.cNumber = cNumber;
        this.vNumber = vNumber;
    }

    private int tabNumber()
    {
        return MainActivity.Tab.GetCurrentTabPosition();
    }

    private void SaveTab()
    {
        final String tabType = (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE) ? "S" : (fragmentType == FRAGMENT_TYPE.PLAN_TYPE) ? "P" : (fragmentType == FRAGMENT_TYPE.FAV_TYPE) ? "F" : "A";
        if (trad == null || trad.equals("")) trad = bbName;
        final CacheTabBO cacheTab = new CacheTabBO(tabNumber(), tabType, tabTitle, searchFullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad);
        _s.SaveCacheTab(cacheTab);
        SetLayoutManager();
    }

    /***
     * Set local bible name
     */
    private void SetLocalBibleName()
    {
        if (bbName == null) bbName = PCommon.GetPrefBibleName(_context);
    }

    private String RollBookName(final String bbName)
    {
        try
        {
            final int INSTALL_STATUS = PCommon.GetInstallStatus(_context);
            switch (INSTALL_STATUS)
            {
                case 6:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("a") == 0) ? "o" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 1:
                {
                    return "k";
                }
                case 2:
                {
                    return (bbName.compareToIgnoreCase("v") == 0) ? "k" : "v";
                }
                case 3:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "k" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : "v";
                }
                case 4:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 5:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }

        return "k";
    }

    /***
     * Get dynamic column count
     * @param ic Info count to display
     * @return Preferred number of columns to use to display the info
     */
    private int GetDynamicColumnCount(final int ic)
    {
        final int dc;

        switch (ic)
        {
            case 1:
            {
                dc = Integer.parseInt(PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_1, "1"));
                break;
            }
            case 2:
            {
                dc = Integer.parseInt(PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_2, "1"));
                break;
            }
            case 3:
            {
                dc = Integer.parseInt(PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_3, "1"));
                break;
            }
            case 4:
            {
                dc = Integer.parseInt(PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_4, "1"));
                break;
            }
            case 5:
            {
                dc = Integer.parseInt(PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_5, "1"));
                break;
            }
            case 6:
            {
                dc = Integer.parseInt(PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_6, "1"));
                break;
            }
            default:
            {
                dc = 1;
                break;
            }
        }

        return dc;
    }

/*
    private void ShowFavSearch()
    {
        try
        {
            final AlertDialog builder = new AlertDialog.Builder(getContext()).create();
            final LayoutInflater inflater = getActivity().getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_fav_search, (ViewGroup) getActivity().findViewById(R.id.llFavOrderBy));

            class InnerClass
            {
                private void OnDismiss(View view)
                {
                    try
                    {
                        final int orderBy = Integer.parseInt(view.getTag().toString());
                        PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.FAV_ORDER, orderBy);
                        builder.dismiss();

                        recyclerViewAdapter = new BibleAdapter(getContext(), bbName, searchFullQuery, orderBy, null);
                        if (WhenTabIsEmptyOrNull(true)) return;
                        recyclerView.setAdapter(recyclerViewAdapter);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.scrollToPosition(scrollPosY);
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebugVersion) PCommon.LogR(view.getContext(), ex);
                    }
                }
            }
            final InnerClass innerClass = new InnerClass();

            final Button btn1 = view.findViewById(R.id.btnFavOrder1);
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    innerClass.OnDismiss(view);
                }
            });
            final Button btn2 = view.findViewById(R.id.btnFavOrder2);
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    innerClass.OnDismiss(view);
                }
            });

            builder.setCancelable(true);
            builder.setView(view);
            builder.setTitle(R.string.favOrderTitle);
            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(getContext(), ex);
        }
    }
*/

    private void SetTabTitle(final String title)
    {
        tabTitle = title;
        MainActivity.Tab.SetCurrentTabTitle(title);
    }

    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    private void SearchBible(final boolean useCache)
    {
        try
        {
            if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                /* if (searchFullQuery != null)
                {
                    if (searchFullQuery.isEmpty()) searchFullQuery = null;
                }
                */

                SaveTab();

                final int orderBy = PCommon.GetFavOrder(_context);
                final int favType = PCommon.GetFavFilter(_context);
                recyclerViewAdapter = new BibleAdapter(getContext(), bbName, searchFullQuery, orderBy, favType);
                if (WhenTabIsEmptyOrNull(true)) return;
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                return;
                //TODO: this is a menu_divider. After I have to create all SearchFav methods or add a param to searchbible to convert the queries
            }
            else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
            {
                //No recycler
                return;
            }

            if (useCache)
            {
                recyclerViewAdapter = new BibleAdapter(_context, tabNumber());
                final int itemCount = recyclerViewAdapter.getItemCount();
                if (itemCount > 0)
                {
                    SetLayoutManager();

                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            SetLocalBibleName();

            boolean isBook = false,  isChapter = false,  isVerse = false;
            int     bNumber = 0,     cNumber = 0,        vNumber = 0;
            @SuppressWarnings("UnusedAssignment") int wCount = 0;
            final String[] words = searchFullQuery.split("\\s");
            final String patternDigit = "\\d+";

            wCount = words.length;

            //--------------------------------------------------------------------------------------
            ShowBookTitle(false);

            if (wCount == 0) return;
            //...so minimum is one

            class InnerClass
            {
                private String MergeWords(final int fromWordPos, final String[] words)
                {
                    final int toWordPos = words.length - 1;

                    String mergedString = "";
                    for(int i=fromWordPos; i <= toWordPos; i++)
                    {
                        if (i != fromWordPos)
                            mergedString = PCommon.ConcaT(mergedString, " ", words[ i ]);
                        else
                            mergedString = PCommon.ConcaT(mergedString, words[ i ]);
                    }

                    return mergedString;
                }
            }

            //Try to get bName and convert it to bNumber
            bNumber = words[ 0 ].matches(patternDigit) ? Integer.parseInt(words[ 0 ]) : _s.GetBookNumberByName( bbName, words[ 0 ] );

            if (wCount == 4)
            {
                if ((words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].matches(patternDigit))
                        || (bNumber > 0 && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].matches(patternDigit)))
                {
                    //<bNumber> <cNumber> <vNumber> <vNumberTo>
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);     //TODO ?  one param less
                    cNumber = Integer.parseInt(words[1]);
                    vNumber = Integer.parseInt(words[2]);
                    final int vNumberTo = Integer.parseInt(words[3]);

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
                    ShowVerses(bbName, bNumber, cNumber, vNumber, vNumberTo);

                    return;
                }
                else if ((words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].equalsIgnoreCase("CR:")))
                {
                    //<bNumber> <cNumber> <vNumber> CR:
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    cNumber = Integer.parseInt(words[1]);
                    vNumber = Integer.parseInt(words[2]);

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                    final String searchExpr = "";
                    if (PCommon._isDebugVersion) System.out.println("SearchExpr: " + searchExpr);

                    SetTabTitle("CR");
                    SaveTab();

                    recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber, vNumber, searchExpr);
                    if (WhenTabIsEmptyOrNull(true)) return;
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            if (wCount == 3)
            {
                //<bNumber> <cNumber> <vNumber>
                if ((words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit)) ||
                        (bNumber > 0 && words[1].matches(patternDigit) && words[2].matches(patternDigit)))
                {
                    isBook = true;
                    isChapter = true;
                    isVerse = true;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    cNumber = Integer.parseInt(words[1]);
                    vNumber = Integer.parseInt(words[2]);

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
                    ShowVerse(bbName, bNumber, cNumber, vNumber);

                    return;
                }
            }

            if (wCount >= 3)
            {
                //<bNumber> <cNumber> <expr>...
                if ((words[0].matches(patternDigit) && words[1].matches(patternDigit)) ||
                        (bNumber > 0 && words[1].matches(patternDigit)))
                {
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    cNumber = Integer.parseInt(words[1]);
                    //noinspection ConstantConditions
                    vNumber = 0;

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                    final InnerClass innerClass = new InnerClass();
                    final String searchExpr = innerClass.MergeWords(2, words);

                    if (PCommon._isDebugVersion) System.out.println("SearchExpr: " + searchExpr);

                    SetTabTitle(searchExpr);
                    SaveTab();

                    recyclerViewAdapter = new BibleAdapter(_context, bbName, bNumber, cNumber, searchExpr);
                    if (WhenTabIsEmptyOrNull(true)) return;
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            if (wCount == 2)
            {
                //<bNumber> <cNumber>
                if (( words[ 0 ].matches(patternDigit) && words[ 1 ].matches(patternDigit) ) ||
                    (bNumber > 0 && words[ 1 ].matches(patternDigit) ))
                {
                    isBook = true;
                    isChapter = true;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[ 0 ]);
                    cNumber = Integer.parseInt(words[ 1 ]);
                    //noinspection ConstantConditions
                    vNumber = 0;

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
                    ShowChapter(bbName, bNumber, cNumber);

                    return;
                }
            }

            if (wCount >= 2)
            {
                //<bNumber> <expr>...
                if ((words[0].matches(patternDigit)) ||
                        (bNumber > 0 ))
                {
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    //noinspection ConstantConditions
                    cNumber = 0;
                    //noinspection ConstantConditions
                    vNumber = 0;

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                    final InnerClass innerClass = new InnerClass();
                    final String searchExpr = innerClass.MergeWords(1, words);

                    if (PCommon._isDebugVersion) System.out.println("SearchExpr: " + searchExpr);

                    SetTabTitle(searchExpr);
                    SaveTab();

                    recyclerViewAdapter = new BibleAdapter(_context, bbName, bNumber, searchExpr);
                    if (WhenTabIsEmptyOrNull(true)) return;
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            //Finally <expr>...
            if (searchFullQuery.length() < searchFullQueryLimit)
            {
                PCommon.ShowToast(_context, R.string.toastEmpty3, Toast.LENGTH_SHORT);
                return;
            }

            isBook = true;
            //noinspection ConstantConditions
            isChapter = false;
            //noinspection ConstantConditions
            isVerse = false;

            bNumber = 0;
            //noinspection ConstantConditions
            cNumber = 0;
            //noinspection ConstantConditions
            vNumber = 0;

            // noinspection ConstantConditions
            SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
            SetTabTitle(searchFullQuery);
            SaveTab();

            recyclerViewAdapter = new BibleAdapter(_context, bbName, searchFullQuery);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(scrollPosY);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }

    private String GetArticleGeneratedSource()
    {
        String source = "";

        try
        {
            for(ShortSectionBO shortSection : lstArtShortSection)
            {
                if (shortSection != null)
                {
                    source = PCommon.ConcaT(source, shortSection.content);
                }
            }
            System.out.println(PCommon.ConcaT("artGeneratedSource: ", source));
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(getContext(), ex);
        }

        return source;
    }

    private ShortSectionBO FindArticleShortSectionByPositionId(final int id)
    {
        ShortSectionBO shortSection = null;

        try
        {
            if (lstArtShortSection == null || lstArtShortSection.size() == 0) return null;

            final int lastElement = lstArtShortSection.size() - 1;
            for (int i = lastElement; i >= 0; i--)
            {
                shortSection = lstArtShortSection.get(i);

                if (id >= shortSection.from_id)
                {
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(getContext(), ex);
        }

        return shortSection;
    }

    /***
     * Move section of article
     * @param fromPositionId    Position in article
     * @param toMoveStep        Step of move
     * @return code or null if not applicable
     */
    private String MoveArticleShortSection(final int fromPositionId, final int toMoveStep)
    {
        if (toMoveStep == 0) return null;

        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        final int toShortPositionId = fromShortPositionId + toMoveStep;
        if (toShortPositionId < 0) return null;
        final String wasToShortSectionContent = lstArtShortSection.get(toShortPositionId).content;

        lstArtShortSection.get(toShortPositionId).content = fromShortSection.content;
        lstArtShortSection.get(fromShortPositionId).content = wasToShortSectionContent;

        return this.GetArticleGeneratedSource();
    }

    /***
     * Delete section of article
     * @param fromPositionId    Position in article
     * @return code or null of not applicable
     */
    private String DeleteArticleShortSection(final int fromPositionId)
    {
        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        lstArtShortSection.remove(fromShortPositionId);

        return this.GetArticleGeneratedSource();
    }

    /***
     * Add section to article
     * @param fromPositionId    Position in article
     * @param content           Content
     * @return code or null of not applicable
     */
    private String AddArticleShortSection(final int fromPositionId, final String content)
    {
        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        final ShortSectionBO newShortSection = new ShortSectionBO(fromShortPositionId, content, fromPositionId);
        lstArtShortSection.add(fromShortPositionId, newShortSection);

        return this.GetArticleGeneratedSource();
    }

    /***
     * Update section of article
     * @param fromPositionId    Position in article
     * @param content           Content
     * @return code or null of not applicable
     */
    private String UpdateArticleShortSection(final int fromPositionId, final String content)
    {
        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        lstArtShortSection.get(fromShortPositionId).content = content;

        return this.GetArticleGeneratedSource();
    }

    /***
     * Show simple edit article dialog
     * @param isUpdate  True for Update, False for Add
     * @param editType  Type: L=Large title, S=Small title, T=Text
     * @param activity
     * @param titleId
     * @param editText
     * @param artId
     * @param position
     */
    @SuppressWarnings("JavaDoc")
    private void EditArticleDialog(final boolean isUpdate, final String editType, final Activity activity, final int titleId, final String editText, final int position, final int artId)
    {
        try
        {
            final LayoutInflater inflater = activity.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_edit_dialog, (ViewGroup) activity.findViewById(R.id.svEdition));
            final TextView tvTitle = view.findViewById(R.id.tvTitle);
            final EditText etEdition = view.findViewById(R.id.etEdition);
            final AlertDialog builder = new AlertDialog.Builder(activity).create();
            builder.setCancelable(true);
            builder.setTitle(titleId);
            builder.setView(view);

            tvTitle.setText(titleId);
            etEdition.setText(editText);

            final Button btnClear = view.findViewById(R.id.btnEditionClear);
            btnClear.setOnClickListener(v -> etEdition.setText(""));
            final Button btnContinue = view.findViewById(R.id.btnEditionContinue);
            btnContinue.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        final String text = etEdition.getText().toString().replaceAll("\n", "<br>").trim();
                        PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.EDIT_DIALOG, text);
                        builder.dismiss();

                        String source;
                        if (isUpdate)
                        {
                            source = UpdateArticleShortSection(position, text);
                        }
                        else
                        {
                            //was: "<br><br><H>"
                            source = editType.equalsIgnoreCase("T")
                                    ? text
                                    : editType.equalsIgnoreCase("L")
                                        ? PCommon.ConcaT("<H>", text, "</H>")
                                        : PCommon.ConcaT("<br><u>", text, "</u><br>");

                            source = AddArticleShortSection(position, source);
                        }

                        if (source != null)
                        {
                            _s.UpdateMyArticleSource(artId, source);
                            onResume();
                        }
                    }
                }, 0);
            });

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(getContext(), ex);
        }
    }
}
