
package org.hlwd.bible;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

public class PreferencesFragment extends PreferenceFragment
{
    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);

            final Preference prefBibleName = findPreference("BIBLE_NAME");
            if (prefBibleName != null)
            {
                prefBibleName.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object o)
                    {
                        final Intent returnIntent = new Intent();
                        getActivity().recreate();
                        getActivity().setResult(Activity.RESULT_OK, returnIntent);

                        return true;
                    }
                });
            }

            final Preference prefThemeName = findPreference("THEME_NAME");
            if (prefThemeName != null)
            {
                prefThemeName.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object o) {

                        final String themeName = o.toString();
                        PCommon.SetThemeName(preference.getContext(), themeName);

                        final Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        getActivity().startActivity(intent);

                        return true;
                    }
                });
            }

            final Preference prefLayoutColumn = findPreference("LAYOUT_COLUMN");
            if (prefLayoutColumn != null) {
                prefLayoutColumn.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object o) {
                        final Intent returnIntent = new Intent();
                        getActivity().setResult(Activity.RESULT_OK, returnIntent);

                        return true;
                    }
                });
            }

            final Preference prefUiLayout = findPreference("UI_LAYOUT");
            if (prefUiLayout != null) {
                prefUiLayout.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object o) {
                        final Intent returnIntent = new Intent();
                        getActivity().setResult(Activity.RESULT_OK, returnIntent);

                        return true;
                    }
                });
            }

            final Preference prefUiLayoutTvBordersWhenSave = findPreference("UI_LAYOUT_TV_BORDERS_SAVE");
            if (prefUiLayoutTvBordersWhenSave != null) {
                prefUiLayoutTvBordersWhenSave.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference)
                    {
                        final Intent returnIntent = new Intent();
                        getActivity().setResult(Activity.RESULT_OK, returnIntent);

                        return false;
                    }
                });
            }

            final Preference prefUiLayoutTvBordersWhenReset = findPreference("UI_LAYOUT_TV_BORDERS_RESET");
            if (prefUiLayoutTvBordersWhenReset != null) {
                prefUiLayoutTvBordersWhenReset.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference)
                    {
                        final Intent returnIntent = new Intent();
                        getActivity().setResult(Activity.RESULT_OK, returnIntent);

                        return false;
                    }
                });
            }

            final Intent returnIntent = new Intent();
            getActivity().setResult(Activity.RESULT_OK, returnIntent);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(getContext(), ex);
        }
    }
}